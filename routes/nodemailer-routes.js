var express = require('express');
var router = express.Router();
const Email = require('../config/email');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');



router.get('/basic', (req, res, next) => {
    let message = {
        to: 'jeffersson_8@hotmail.com',
        subject: 'Email con nodemailer',
        html: '<p> hola con nodemailer + express</p>'
    };
    Email.transporter.sendMail(message, (error, info) => {
        if (error) {
            res.status(500).send(error.message);
        }
        Email.transporter.close();
        res.send('respuesta del servidor "%s"' + info.response);
    });
});

router.get('/attachment', (req, res, next) => {
    let message = {
        to: 'jeffersson_8@hotmail.com',
        subject: 'Email con nodemailer',
        html: '<p> hola con nodemailer + express</p> <p><img src="cid:node-js"/></p>',
        attachments: [{
                filename: 'node-js.png',
                path: __dirname + '/../uploads/nodejs.png',
                cid: 'node-js'
            },
            {
                filename: 'node-js2.png',
                path: __dirname + '/../uploads/nodejs2.png'
            }
        ]
    };
    Email.transporter.sendMail(message, (error, info) => {
        if (error) {
            res.status(500).send(error.message);
        }
        Email.transporter.close();
        res.send('respuesta del servidor "%s"' + info.response);
    });
});


router.get('/handlebars', (req, res, next) => {

    Email.transporter.use('compile', hbs({
        viewEngine: 'hbs',
        extName: '.hbs',
        viewPath: path.join(__dirname, '../views/email-templates')
    }));

    let message = {
        to: 'jeffersson_8@hotmail.com',
        subject: 'Email con nodemailer',
        template: 'test',
        context: {
            text: 'hola con nodemailer + express node js'
        },
        attachments: [{
                filename: 'node-js.png',
                path: __dirname + '/../uploads/nodejs.png',
                cid: 'node-js'
            },
            {
                filename: 'node-js2.png',
                path: __dirname + '/../uploads/nodejs2.png'
            }
        ]
    };
    Email.transporter.sendMail(message, (error, info) => {
        if (error) {
            res.status(500).send(error.message);
        }
        Email.transporter.close();
        res.send('respuesta del servidor "%s"' + info.response);
    });
});

module.exports = router;