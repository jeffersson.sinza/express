var express = require('express');
var router = express.Router();

router.get('/:filename/:extension', (req, res, next) => {
    const options = {
        root: __dirname + '/../uploads',
        headers: {
            'x-timestamp': Date.now()
        }
    };
    const filename = `${req.params.filename}.${req.params.extension}`;
    res.sendFile(filename, options, (error) => {
        if (error) {
            next(error);
        } else {
            console.log('send:' + filename);
        }
    });
});

module.exports = router;