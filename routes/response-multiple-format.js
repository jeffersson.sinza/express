var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
    res.format({
        text: () => {
            res.send('hola');
        },
        html: () => {
            res.send('<p>hola</p>');
        },
        json: () => {
            res.send({ message: 'hola' });
        },
        xml: () => {
            res.send('<messages><message>hola</message></messages>');
        },
        default: () => {
            res.status(406).send('Formato no valido');
        }

    });
});

module.exports = router;