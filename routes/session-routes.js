var express = require('express');
var router = express.Router();


router.get('/', (req, res, next) => {
    res.status(200).json(req.session.username || 'no existe la session');
});
router.get('/create', (req, res, next) => {
    req.session.username = 'IParra';
    res.redirect('/session_routes');
});
router.get('/removekey', (req, res, next) => {
    req.session.username = 'null';
    res.redirect('/session_routes');
});
router.get('/destroy', (req, res, next) => {
    req.session.destroy();
    res.redirect('/session_routes');
});
module.exports = router;