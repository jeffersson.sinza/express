var express = require('express');
var routes = express.Router();

routes.get('', (req, res, next) => {
    res.render('i18n', {
        title: 'i18n'
    })
});

module.exports = routes;