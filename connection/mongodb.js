const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_paginate = require('mongoose-paginate');

const bookSchema = new Schema({
    isbn: { type: String, index: true },
    title: { type: String, index: true },
    body: { type: String, index: false }
}, {
    timestamps: {
        createdAt: 'created_at'
    }
});

bookSchema.plugin(mongoose_paginate);
const Book = mongoose.model('book', bookSchema);
mongoose.connect('mongodb://127.0.0.1/nodejs-course');
module.exports = {
    Book
};