var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var user = require('../models/user');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    user.findById(id, (error, user) => {
        done(error, user[0]);
    });
});

router.get('/login', (req, res, next) => {
    res.render('login', {
        title: 'login passport y mysql',
        message: req.flash('error')
    });
});



passport.use(new LocalStrategy(
    (username, password, done) => {
        user.findOne(username, password, (error, user) => {
            if (error) {
                return done(error);
            }
            if (!user) {
                return done(null, false, {
                    message: 'El usuario no ha sido identificado'
                });
            }
            return done(null, user);
        });
    }
));


router.post('/login', passport.authenticate('local', {
    failureRedirect: '/login_routes/login',
    failureFlash: true
}), (req, res) => {
    res.redirect(`/login_routes/users/${req.user.id}`);
});

router.get('/users/:id', (req, res, next) => {
    res.render('user', {
        title: `bienvenidos ${req.user.username}`
    });
});

router.get('/logout', (req, res, next) => {
    if (req.user) {
        req.logout();
    }
    res.redirect('/login_routes/login');
});

module.exports = router;