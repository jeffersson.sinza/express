var express = require('express');
const router = express.Router();
const User = require('../models/user');
const paginate = require('express-paginate');

router.get('/', (req, res, next) => {
    let page = (parseInt(req.query.page) || 1) - 1;
    let limit = 2;
    let offset = page * limit;
    User.paginate(offset, limit, (error, users) => {
        if (error) {
            return res.status(500).send(error);
        }
        const currentPage = offset === 0 ? 1 : (offset / limit) + 1;
        const totalCount = users.count[0].total;
        const pageCount = Math.ceil(totalCount / limit);
        const pagination = paginate.getArrayPages(req)(10, pageCount, currentPage);

        res.render('users-paginated', {
            users: users.rows,
            currentPage,
            links: pagination,
            hasNext: paginate.hasNextPages(pageCount),
            pageCount
        });
    });
});

module.exports = router;