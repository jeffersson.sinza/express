var express = require('express');
const router = express.Router();


router.get('/:filename/:extension', (req, res, next) => {
    const filePath = `./uploads/${req.params.filename}.${req.params.extension}`;
    const filename = `${req.params.filename}.${req.params.extension}`;
    res.download(filePath, filename, (error) => {
        if (error) {
            next(error);
        } else {
            console.log('Archivo descargado');
        }
    });

});
module.exports = router;