require('./connection/mongodb');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var localStrategy = require('passport-local');
var hbs = require('hbs');
var hbsUtils = require('hbs-utils')(hbs);
var i18n = require('i18n');
var paginate = require('express-paginate');
var methodOverride = require('method-override');
hbsUtils.registerPartials(path.join(__dirname, 'views/partials'));
hbsUtils.registerWatchedPartials(path.join(__dirname, 'views/partials'));
require('./helpers/hbs')(hbs);

// routes  

var index = require('./routes/index');
var users = require('./routes/users');
var invoices = require('./routes/invoices');
var middleware_routes = require('./routes/middleware-routes');
var filesystem_routes = require('./routes/filesystem-routes');
var query_string = require('./routes/query-string');
var session_routes = require('./routes/session-routes');
var session_flash_routes = require('./routes/session-flash-routes');
var users_mysql_routes = require('./routes/users-mysql-routes');
var form_validation_routes = require('./routes/form-validation-routes');
var login_routes = require('./routes/login-routes');
var api_Rest_users_mysql = require('./routes/api-rest-users-mysql');
var xml_router = require('./routes/xml-router');
var multer = require('./routes/multer');
var i18n_routes = require('./routes/i18n-routes');
var nodemailer_routes = require('./routes/nodemailer-routes');
var response_multiple_format = require('./routes/response-multiple-format');
var response_file = require('./routes/response-file');
var downloader_file = require('./routes/downloader-file');
var handlebars_helpers = require('./routes/handlebars-helpers');
var mysql_pagination = require('./routes/mysql_pagination');
var books_mongoose = require('./routes/books-mongoose');
var async = require('./routes/async');
var app = express();
app.use(paginate.middleware(2, 20));

//configurar   el idioma
i18n.configure({
    locales: ['es', 'en'],
    cookie: 'secret-lang',
    directory: path.join(__dirname, 'locales'),
    defaultLocale: 'es'
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(methodOverride('_method'));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: '<mysecret>',
    name: 'super-secret-cookie-name',
    resave: true,
    saveUninitialized: true
}));

app.use(flash());

app.get('/locale/:lang', (req, res, next) => {
    res.cookie(
        'secret-lang', req.params.lang
    );
    res.redirect('/i18n_routes');
});
app.use(i18n.init);
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
    res.locals.user = req.user;
    next();
});
app.use('/components', express.static(path.join(__dirname, 'public/components')));

app.use('/', index);
app.use('/users', users);
// interceptar  todos  los  peticiones http al recurso
app.all('/invoices/*', (req, res, next) => {
    if (!req.headers['api-key']) {
        res.status(200).json('api key is mandatory');
        return;
    }
    next();
});
app.use('/invoices', invoices);
app.use('/middleware-routes', middleware_routes);
app.use('/filesystem_routes', filesystem_routes);
app.use('/query_string', query_string);
app.use('/session_routes', session_routes);
app.use('/session_flash_routes', session_flash_routes);
app.use('/users_mysql_routes', users_mysql_routes);
app.use('/form_validation_routes', form_validation_routes);
app.use('/login_routes', login_routes);
app.use('/api_Rest_users_mysql', api_Rest_users_mysql);
app.use('/xml_router', xml_router);
app.use('/multer-routes', multer);
app.use('/i18n_routes', i18n_routes);
app.use('/nodemailer_routes', nodemailer_routes);
app.use('/response_multiple_format', response_multiple_format);
app.use('/response_file', response_file);
app.use('/downloader_file', downloader_file);
app.use('/handlebars_helpers', handlebars_helpers);
app.use('/mysql_pagination', mysql_pagination);
app.use('/books_mongoose', books_mongoose);
app.use('/async', async);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;