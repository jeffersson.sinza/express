const express = require('express');
const router = express.Router();
const waterfall = require('async/waterfall');
const each = require('async/each');

router.get('/', (req, res, next) => {
    waterfall([
        (callback) => {
            callback(null, [1, 2, 3, 4, 5]);
        },
        (array, callback) => {
            if (array.length == 5) {
                each(array, (data, done) => {
                    if (isNaN(data)) {
                        done('data isNAN');
                    } else {
                        console.log(data);
                        done();
                    }
                }, (error, sucess) => {
                    if (!error) {
                        callback(null, array);
                    } else {
                        callback(error);
                    }
                });
            } else {
                callback("error array.length");
            }
        }, (array, callback) => {
            callback(null, array.reduce((a, b) => a + b, 0));
        }
    ], (error, result) => {
        if (error) {
            res.json(error);
        } else {
            res.json(result);
        }
    });
});

module.exports = router;