var express = require('express');
var router= express.Router();

router.get('/',(req,res,next)=>{
    res.send(req.flash('info'));
});

router.get('/create',(req,res,next)=>{
   req.flash('info','Session flash creada');
   res.redirect('/session_flash_routes');
})

module.exports=router;