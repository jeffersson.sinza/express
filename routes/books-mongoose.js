const express = require('express');
const router = express.Router();
const Book = require('../connection/mongodb').Book;
const moment = require('moment');

const form = require("express-form2");
const field = form.field;

const paginate = require('express-paginate');

router.get('/', (req, res, next) => {
    Book.paginate({}, { page: req.query.page, limit: req.query.limit }, (error, result) => {
        let books = [];

        if (result.docs.length > 0) {
            result.docs.map((book) => {
                books.push({
                    _id: book._id,
                    isbn: book.isbn,
                    title: book.title,
                    body: book.body,
                    createdAt: moment(book.createdAt).format('DD-MM-YYYY')
                });
            });
        }

        res.render('books/index', {
            title: 'Books List',
            books,
            message: req.flash('success'),
            pageCount: result.pages,
            itemCount: result.total,
            links: paginate.getArrayPages(req)(5, result.pages, req.query.page)
        });
    });
});

router.get('/add', (req, res, next) => {
    res.render('books/form', {
        title: 'Nuevo libro',
        message: req.flash('success')
    });
});

router.post(
    '/',
    (req, res, next) => {
        req.body = JSON.parse(JSON.stringify(req.body));
        next();
    },
    form(
        field("isbn")
        .trim()
        .required("", "El %s es requerido"),

        field('title')
        .trim()
        .required('', 'El campo %s es requerido'),

        field('body')
        .trim()
        .required('', 'El %s es requerido')
    ),
    (req, res, next) => {
        if (!req.form.isValid) {
            res.render('books/form', {
                title: 'Nuevo libro',
                errors: req.form.errors,
                book: req.body
            });
        } else {
            new Book({
                isbn: req.body.isbn,
                title: req.body.title,
                body: req.body.body,
                createdAt: Date.now()
            }).save((error, book, count) => {
                req.flash('success', 'El libro se ha creado correctamente');
                res.redirect('/books_mongoose/add');
            });
        }
    }
);

router.get('/edit/:id', (req, res, next) => {
    Book.findById(req.params.id, (error, book) => {
        res.render('books/form', {
            title: 'Actualizar libro',
            book,
            is_update: true,
            message: req.flash('success')
        });
    });
});

router.put('/:id', (req, res, next) => {
    Book.findOneAndUpdate({ _id: req.params.id }, req.body).then((error, response) => {
        req.flash("success", "El libro se ha actualizado correctamente");
        res.redirect(`/books_mongoose/edit/${req.params.id}`);
    });
});

router.delete('/destroy/:id', (req, res, next) => {
    Book.findOneAndRemove({ _id: req.params.id }).then((error, response) => {
        req.flash("success", "El libro se ha eliminado correctamente");
        res.redirect('/books_mongoose');
    });
});

module.exports = router;