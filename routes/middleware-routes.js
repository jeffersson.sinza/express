var express = require('express');
var router = express.Router();

const mA = (req, res, next) => {
    console.log('mA');
    next();
};
const mB = (req, res, next) => {
    console.log('mB');
    next();
};
const mC = (req, res, next) => {
    res.send('mC');

};

router.get('/', [mA, mB, mC]);

module.exports = router;