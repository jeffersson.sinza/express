var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/folders', (req, res, next) => {
    let files = [];
    fs.readdirSync('/xampp/img').filter((file) => {
        files.push(file);
    });
    res.status(200).json(files);
});
router.get('/files', (req, res, next) => {
    let files = [];
    fs.readFile('app.js', 'utf8', (error, data) => {
        if (error) {
            res.status(500).send(error);
        }
        res.status(200).send(data);
    })
});

router.get('/write', (req, res, next) => {
    fs.writeFile('test.js', 'console.log("prueba")', (error) => {
        if (error) {
            res.status(500).send(error);
        }
        res.status(200).json("el archivo se creo");
    })
});

module.exports = router;