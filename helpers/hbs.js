module.exports = (hbs) => {
    hbs.registerHelper('for', (from, to, increment, options) => {
        let total = '';
        for (let i = from; i < to; i += increment) {
            total += options.fn(i);
        }
        return total;
    });

    hbs.registerHelper('math', (a, operator, b) => {
        a = parseFloat(a);
        b = parseFloat(b);

        return {
            "+": a + b,
            "-": a - b,
            "*": a * b,
            "/": a / b,
            "%": a % b
        }[operator];
    });

    hbs.registerHelper('if_eq', (a, b, options) => {
        if (a === b) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    hbs.registerHelper('is_active', (index, number) => {
        return (index + 1) == number ? 'active' : '';
    });

    hbs.registerHelper('prev_link', (pagination) => {
        return pagination.href(true);
    });

    hbs.registerHelper('next_link', (pagination) => {
        return pagination.href();
    });

    hbs.registerHelper('has_next_links', (pageCount, pagination, options) => {
        if (pagination.hasNextPages(pageCount)) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
};