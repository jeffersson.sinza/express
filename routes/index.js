var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express sinza ' });
});

router.get('/hola-mundo', function(req, res, next) {
    res.json({ msg: "hola mundo" });
});

router.get('/change-layout', function(req, res, next) {
    res.render('change-layout', {
        title: 'change-layout',
        page: 'variable page',
        layout: 'handlebars-layout'
    });
});

router.get('/handlebars', function(req, res, next) {
    res.render('handlebars', {
        user: [{ id: 1, name: 'sinza' }, { id: 2, name: 'juan' }],
        owner: { firstName: 'Jeffersson', lastName: 'sinza' },
        appName: 'sinza',
        layout: 'handlebars-layout'
    })
});
module.exports = router;