var express = require('express');
var router = express.Router();
var form = require('express-form2');
var field = form.field;
var User = require('../models/user');
var bycrypt = require('bcrypt-nodejs');


router.get('', (req, res, next) => {
    res.render("form-validation", {
        title: "validacion del formularios",
        erros: [],
        post: [],
        flash: req.flash('success')
    });
});

router.post('/register',
    (req, res, next) => {
        req.body = JSON.parse(JSON.stringify(req.body));
        next();
    }, form(
        field("username")
        .trim()
        .required("", "El %s es requerido")
        .is(/^[a-z]+$/, "El %s sólo puede contener letras"),
        field("email")
        .trim()
        .required("", "El %s es requerido")
        .isEmail('El formato de %s no es valido'),
        field('password')
        .trim()
        .required()
        .minLength(6, "El password  no puede contener menos de 6 caracteres")
        .is(/^[0-9]+$/, 'El %s sólo puede contener numeros'),
        field('confirm-password')
        .equals('field::password', 'Los passwords no coninciden')
    ), (req, res, next) => {
        if (!req.form.isValid) {
            res.render('form-validation', {
                title: 'validacion de formulario',
                errors: req.form.errors,
                post: req.body
            });
        } else {
            const user = {
                id: null,
                username: req.body.username,
                password: bycrypt.hashSync(req.body.password),
                email: req.body.email
            };
            User.insert(user, (error, insertId) => {
                if (insertId) {
                    req.flash('success', 'usuarios registrado correctamente');
                    res.redirect('/form_validation_routes');
                } else {
                    res.status(500).send('error guardando el usuario' || error);
                }
            });
        }
    });

module.exports = router;