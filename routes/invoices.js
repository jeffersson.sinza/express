var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
        res.json(200, 'Obtener todas las facturas');
    })
    .get('/:invoice', (req, res, next) => {
        res.json(200, 'Obtener todas las facturas ' + req.params.invoice);
    })
    .post('/', (req, res, next) => {
        res.json(req.body);
    })
    .delete('/:invoice', (req, res, next) => {
        res.json(200, 'eliminar facturas');
    })
    .patch('/:invoice', (req, res, next) => {
        res.json({
            body: req.body,
            params: req.params
        });
    });

module.exports = router;