var conn = require('../connection/mysql');
var bycrypt = require('bcrypt-nodejs');

let user = {};

user.fetchAll = (callback) => {
    if (conn) {
        const sql = "SELECT  * FROM users";
        conn.query(sql, (error, rows) => {
            if (error) {
                return callback(error);
            }
            return callback(null, rows);
        });
    } else {
        return callback("No se ha podido conectar");
    }
};
user.insert = (user, callback) => {
    user.password = bycrypt.hashSync(user.password);
    if (conn) {
        const sql = "INSERT INTO users SET ?";
        conn.query(sql, [user], (error, result) => {
            if (error) {
                return callback(error);
            }
            return callback(null, result.insertId);
        });
    } else {
        return callback("No se ha podido conectar");
    }
};
user.findById = (id, callback) => {
    if (conn) {
        const sql = "Select * from users where id = ?";
        conn.query(sql, [id], (error, rows) => {
            if (error) {
                return callback(error);
            }
            return callback(null, rows);
        });
    } else {
        return callback("No se ha podido conectar");
    }
};
user.findOne = (username, password, callback) => {
    if (conn) {
        conn.query(`Select  * from users where username = ${conn.escape(username)} `, (error, rows) => {
            if (error) {
                return callback(error);
            }
            if (rows.length === 0) {
                return callback(null, null);
            }
            var check = bycrypt.compareSync(password, rows[0].password);
            if (check) {
                return callback(null, rows[0]);
            } else {
                return callback(null, null);
            }

        });
    }
};

user.update = (user, callback) => {
    if (conn) {
        const sql = "update users  set username= ? and email= ? and  password=? where id = ?";
        conn.query(sql, [user.username, user.email, bycrypt.hashSync(user.password), user.id], (error, result) => {
            if (error) {
                return callback("error actualizando usuario");
            } else {
                return callback(null, "usuario actualizado");
            }

        });
    } else {
        return callback("No se ha podido conectar");
    }
};
user.delete = (user, callback) => {
    if (conn) {
        const sql = "delete  from users where id = ?";
        conn.query(sql, [user.id], (error, result) => {
            if (error) {
                return callback("error ha eliminar  usuario");
            } else {
                return callback(null, "usuario eliminado");
            }
        });
    } else {
        return callback("No se ha podido conectar");
    }
};

user.paginate = (offset, limit, callback) => {
    if (conn) {
        conn.query('select * from users limit ?,?', [offset, limit], (error, rows) => {
            if (error) {
                callback(error);
            } else {
                conn.query('select count(*) total from users', (error, count) => {
                    return callback(null, {
                        count,
                        rows
                    });
                });
            }
        });
    }
};

user.response = (res, error, data) => {
    if (error) {
        res.status(500).json(error);
    } else {
        res.status(200).json(data);
    }
};

module.exports = user;